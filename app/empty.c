
/*
 *  ======== empty.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/I2C.h>
// #include <ti/drivers/SPI.h>
// #include <ti/drivers/Watchdog.h>

/* Driver configuration */
#include "ti_drivers_config.h"

#ifdef __ICCARM__
    #include <DLib_Threads.h>
#endif

/* POSIX Header files */
#include <pthread.h>
#include <mqueue.h>

/* RTOS header files */
#include <FreeRTOS.h>
#include <task.h>

#include <ti/drivers/Board.h>
#include <ti/bleapp/ble_app_util/inc/bleapputil_api.h>
#include <bleapputil_internal.h>
extern mqd_t mqdes_1;

uint32_t interval = 100;


/* Stack size in bytes */
#define THREADSTACKSIZE 1024

extern void myHandler(char *pCharValue);

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    /* 500 mili second delay */
    uint32_t time = 200000;
    char msg[48] = {0};
    memset(msg, 0, 48);

    /* Call driver init functions */
    GPIO_init();
    // I2C_init();
    // SPI_init();
    // Watchdog_init();

    /* Configure the LED pin */
    GPIO_setConfig(CONFIG_GPIO_LED_RED, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Turn on user LED */
    GPIO_write(CONFIG_GPIO_LED_RED, CONFIG_GPIO_LED_ON);
    char *pCharValue4 = (char *)BLEAppUtil_malloc(sizeof(char));
    *pCharValue4 = 0;

    while (1)
    {
        if(mq_receive(mqdes_1, (char *)&msg, 48, NULL) > 0 ){
            interval=(uint32_t)(msg[47]*3.8);
        }
        usleep(interval*1000);
        GPIO_toggle(CONFIG_GPIO_LED_RED);
        *pCharValue4+=1;
        BLEAppUtil_invokeFunction(myHandler, pCharValue4);
    }
}

void emptyMain(void)
{
    pthread_t thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    /* Initialize the attributes structure with default values */
    pthread_attr_init(&attrs);

    /* Set priority, detach state, and stack size attributes */
    priParam.sched_priority = 5; // Lower the priority of this task
    retc = pthread_attr_setschedparam(&attrs, &priParam);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0)
    {
        /* failed to set attributes */
        while (1) {}
    }

    retc = pthread_create(&thread, &attrs, mainThread, NULL);
    if (retc != 0)
    {
        /* pthread_create() failed */
        while (1) {}
    }
}



